# Dockhere (c) 2019 Ian Norton

`dockhere` is a python program aimed at reducing what you type to use
docker containers as though they were shells.

## Running

```
python -m dockhere IMAGE bash
```

Will give you a bash shell inside the given docker image, inside your current
directory and as your current logged in user. eg:

```
~/git/dockhere $ python -m dockhere alpine:latest
/home/inb/git/dockhere $ ls -l
total 16
-rw-r--r--    1 inb      10116           94 Jul  4 09:22 MANIFEST
drwxr-xr-x    2 inb      10116         4096 Jul  4 09:22 dist
drwxr-xr-x    3 inb      10116         4096 Jul  4 13:28 dockhere
-rw-r--r--    1 inb      10116          420 Jul  4 09:20 setup.py
/home/inb/git/dockhere $ whoami
inb
/home/inb/git/dockhere $ pwd
/home/inb/git/dockhere
/home/inb/git/dockhere $

$ cat /etc/issue 
Welcome to Alpine Linux 3.9
Kernel \r on an \m (\l)

```

## Installing

Simply:

```
$ python3 -m pip install dockhere
```

# How it Works

`dockhere` will start a container with your current directory bind-mounted
inside and setup a user that matches your UID and name.  It will then pass the
remaing command line arguments after `IMAGE` do `docker exec`

# Privileged Mode or Root

`dockhere` by default drops you into an un-priv container as your own user, You
can supply `--root` to start as root (usually the default with common docker 
images) and also supply `--priv` if you need to use a privileged container to
do thigs like mounting loopback filesystems.
